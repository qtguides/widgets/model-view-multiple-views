#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QStringListModel>

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = nullptr);
    ~Dialog();

private slots:
    void on_pushButtonAgregar_clicked();
    void on_pushButtonInsertar_clicked();
    void on_pushButtonEliminar_clicked();

private:
    Ui::Dialog *ui;
    QStringListModel *modelo;
};

#endif // DIALOG_H
