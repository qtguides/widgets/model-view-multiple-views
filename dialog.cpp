#include "dialog.h"
#include "ui_dialog.h"

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);

    QStringList datos;
    datos.push_back("Hola");
    datos.push_back("Como");
    datos.push_back("Va");

    modelo = new QStringListModel(this);
    modelo->setStringList(datos);

    ui->listView->setModel(modelo);
    ui->comboBox->setModel(modelo);

    // Entrar a Edit Mode al presionar cualquier tecla o hacer doble click
    ui->listView->setEditTriggers(QAbstractItemView::AnyKeyPressed | QAbstractItemView::DoubleClicked);

}

Dialog::~Dialog()
{
    delete ui;
}

/**
 * @brief Agregar un item al final del listView, lo selecciona, y comienza el modo edicion.
 */
void Dialog::on_pushButtonAgregar_clicked()
{
    // Obtenemos la cantidad de filas que tiene el modelo
    int row = modelo->rowCount();

    // Insertamos un elemento
    modelo->insertRows(row, 1);

    // Obtenemos el indice de la fila recien insertada
    QModelIndex index = modelo->index(row);

    // Seleccionamos y editamos dicha fila
    ui->listView->setCurrentIndex(index);
    ui->listView->edit(index);
}

/**
 * @brief Inserta un item a continuacion del elemento seleccionaod en el listView, lo selecciona, y comienza el modo edicion.
 */
void Dialog::on_pushButtonInsertar_clicked()
{
    // Obtenemos la cantidad de filas que tiene el modelo
    int row = ui->listView->currentIndex().row();

    // Insertamos un elemento
    modelo->insertRows(row, 1);

    // Obtenemos el indice de la fila recien insertada
    QModelIndex index = modelo->index(row);

    // Seleccionamos y editamos dicha fila
    ui->listView->setCurrentIndex(index);
    ui->listView->edit(index);
}

void Dialog::on_pushButtonEliminar_clicked()
{
    // Obtenemos el nro de fila seleccionado en el listView para eliminar
    int row = ui->listView->currentIndex().row();

    // Eliminarmos
    modelo->removeRows(row, 1);
}
